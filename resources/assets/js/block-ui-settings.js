require('block-ui');

(function() {
    "use strict";

    $.blockUI.defaults = {
        message:  '<div><div class="sk-swing"><div class="sk-swing-dot"></div><div class="sk-swing-dot"></div></div></div>',

        css: {
            padding:        0,
            margin:         0,
            top:            '50%',
            left:           '50%',
            cursor:         'wait',
            transform:      'translate(-50%,-50%)',
            'z-index':         '9999',
        },

        showOverlay: true,

        overlayCSS:  {
            backgroundColor: '#fff',
            opacity:         0.6,
            cursor:          'wait'
        },
    }
})();
