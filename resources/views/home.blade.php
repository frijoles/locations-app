@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="app"></div>
    </div>
@endsection

@push('scripts')
    <script>var gmk = '{{ config('app.google_maps_ket') }}'</script>
    <script src="{{ asset('js/app.js') }}"></script>
@endpush
