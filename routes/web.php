<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Authentication Routes */
Route::group(['namespace' => 'Auth'], function() {

    /** Sign In */
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('login');

    /** Sign Out */
    Route::post('/logout', 'LoginController@logout')->name('logout');

    /** Register */
    Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'RegisterController@register')->name('register');
});

/** Locations App Routes */
Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'locations', 'as' => 'locations'], function() {
        Route::get('/', 'LocationsController@locations');
        Route::post('/add', 'LocationsController@add')->name('.add');
    });
});
