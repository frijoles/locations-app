# Locations App

Locations app build with Laravel framework and VueJs. Users can register, login and submit new locations from google autocomple search field
or from the client current position (an approval from the browser is required).

## Installation
First Install the application dependencies
```
composer install
```

As always prepare your .env file. You can copy .env.example and update the necessary attributes that required by the app:
```
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

GOOGLE_MAPS_SECRET_KEY=
```
Generate your `APP_KEY` by running `php artisan generate:key`

Migrate the application database tables, `users` and `locations`.
```$xslt
php artisan migrate
``` 

That is it, if every installed and migrated as it should you can run the application.
Run `php artisan server` that will create instantly web server for the app in port 8000, `http://localhost:8000`,
or enter from exists web server directed to `{APP_PATH}/public/`.
