<?php

namespace App\Http\Controllers;

use App\Models\UserLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LocationsController extends Controller
{
    /**
     * Append new place to user locations.
     *
     * @return array
     */
    public function add(Request $request)
    {
        $user = Auth::user();

        $place = $request->validate([
            'lat' => 'required',
            'lng' => 'required',
            'address' => 'required',
        ]);

        if (UserLocation::where('user_id', $user->id)->where('lat', $place['lat'])->where('lng', $place['lng'])->count()) {
            return response()->json(['success' => false, 'message' => 'Location already exists.'], 422);
        }

        $location = UserLocation::create(array_merge($place, ['user_id' => $user->id]));
        return response()->json(['success' => true, 'location' => $location]);
    }

    /**
     * Get the user locations.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function locations()
    {
        $user = Auth::user();
        return response()->json(['success' => true, 'locations' => $user->locations]);
    }
}
