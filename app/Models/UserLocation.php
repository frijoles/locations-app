<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model
{
    protected $table = 'user_locations';

    protected $fillable = [
        'lat', 'lng', 'address', 'user_id'
    ];

    /**
     * Return the location user owner.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
